/*
 * Copyright 2013 Blue Lotus Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * $Id$
 */
package com.bluelotussoftware.myfaces.rf.example;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author John Yeary
 * @version 1.0
 */
@ManagedBean
@RequestScoped
public class IndexBean {

    private static final String INFO_MESSAGE = "This is an informational message.";
    private static final String WARNING_MESSAGE = "This is an warning message.";
    private static final String ERROR_MESSAGE = "This is an error message.";
    private static final String FATAL_MESSAGE = "This is an fatal message.";

    public IndexBean() {
    }

    public void createMessages() {
        info();
        warn();
        error();
        fatal();
    }

    public void info() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO", "(" + FacesMessage.SEVERITY_INFO.getOrdinal() + ") " + INFO_MESSAGE));
    }

    public void warn() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "WARN", "(" + FacesMessage.SEVERITY_WARN.getOrdinal() + ") " + WARNING_MESSAGE));
    }

    public void error() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR", "(" + FacesMessage.SEVERITY_ERROR.getOrdinal() + ") " + ERROR_MESSAGE));
    }

    public void fatal() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "FATAL", "(" + FacesMessage.SEVERITY_FATAL.getOrdinal() + ") " + FATAL_MESSAGE));
    }
}
