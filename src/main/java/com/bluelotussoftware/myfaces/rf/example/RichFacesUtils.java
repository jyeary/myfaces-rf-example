/*
 * Copyright 2013 Blue Lotus Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * $Id$
 */
package com.bluelotussoftware.myfaces.rf.example;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

/**
 * <p>
 * A utility class to implement convenience methods, and integration between JSF
 * RI implementations.</p>
 * <p>
 * <strong>Note: </strong> This should be changed to {@code Named} for CDI.</p>
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
@ManagedBean
public class RichFacesUtils {

    /**
     * This method is used in the CSS file to alter the values of the icon URLs
     * based on whether the underlying {@link FacesMessage#SEVERITY_INFO} value
     * starts with 0 (Mojarra), or a 1 (MyFaces).
     *
     * @param level The {@link FacesMessage.Severity#getOrdinal()) to be evaluated.
     * @return a RichFaces resource URL for the appropriate notification.
     */
    public String notificationIconURL(int level) {
        if (FacesMessage.SEVERITY_INFO.getOrdinal() == 0) {
            ++level;
        }
        String path = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
        switch (level) {
            case 1: {
                //INFO
                return path + "/faces/javax.faces.resource/info.png?ln=org.richfaces";
            }
            case 2: {
                // WARN
                return path + "/faces/javax.faces.resource/warning.png?ln=org.richfaces";
            }
            case 3: {
                //ERROR
                return path + "/faces/javax.faces.resource/error.png?ln=org.richfaces";
            }
            case 4: {
                //FATAL
                return path + "/faces/javax.faces.resource/fatal.png?ln=org.richfaces";
            }
            default: {
                //INFO
                return path + "/faces/javax.faces.resource/info.png?ln=org.richfaces";
            }
        }
    }
}
